// Packages
import config from "@acai/config";

// Middlewares
import ApiMiddleware		from "../app/middlewares/api.middleware";
import AuthMiddleware		from "../app/middlewares/auth.middleware";
import bodyParserMiddleware	from "../app/middlewares/body.middleware";

// -------------------------------------------------
// Global middlewares
// -------------------------------------------------

config.setConfig("global", [
	bodyParserMiddleware,
	ApiMiddleware
]);

// -------------------------------------------------
// Aliased middlewares
// -------------------------------------------------

config.setConfig("middleware", {
	"auth": AuthMiddleware,
});