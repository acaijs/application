// Packages
import test from "@acai/testing";
import Server from "@acai/server";

// Utils
import generateServer 	from "../utils/server";
import request 			from "../utils/request";
import resetMigrations	from "../utils/migration";

test.group("Auth tests", async (ctx) => {
	// general instance to be reused and dispatched
	let server: Server;

	// make sure no server instances are kept running after each test
	ctx.beforeAll(async () => { server = await generateServer(); });
	ctx.beforeEach(async () => {if (server) await server.run(); });
	ctx.afterEach(async () => { if (server) await server.stop(); });

	// make sure to clear database between sessions
	ctx.afterEach(async () => resetMigrations());

	test.group("Register requests", () => {
		test("Succesful register", async (expect) => {
			// arrange
			const name		= "John Doe";
			const email 	= "test@email.com";
			const password 	= "a1S@asdf";

			// act
			const { body } = await request.post("/register").send({ name, email, password, password_confirmation: password });

			// assert
			expect(body.token).toBeDefined();
			expect(body.data).toBeDefined();
			expect(body.data.name).toBe(name);
			expect(body.data.email).toBe(email);
		});
	}).tag("register");
}).tag(["auth", "resource"]);