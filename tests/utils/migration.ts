// Packages
import config 							from "@acai/config";
import { getModels }					from "@acai/model";
import test from "@acai/testing";
import * as fs 							from "fs";
import * as path 						from "path";

export default async function resetMigrations () {
	// get model files
	const files = fs.readdirSync(config.config.paths.models, {
		encoding: "utf-8",
		withFileTypes: true,
	});

	// load all models
	for await (const dirEntry of files) {
		if (dirEntry.isFile()) {
			await import (path.join("../../",config.config.paths.models, dirEntry.name));
		}
	}

	await Promise.all(getModels().map(async i => {
		test.cache({table: `${i.$table} - before delete`, value: !!(await i.query().existsTable())});
		await i.query().raw("SET FOREIGN_KEY_CHECKS=0");
		await i.query().dropTable();
		await i.query().raw("SET FOREIGN_KEY_CHECKS=1");
		test.cache({table: `${i.$table} - after delete`, value: !!(await i.query().existsTable())});
	}));
}