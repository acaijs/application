// Packages
import Server from "@acai/server";
import config from "@acai/config";

// interfaces
import ProviderInterface 	from "@acai/server/src/interfaces/provider";
import MiddlewareType 		from "@acai/server/src/interfaces/middleware";

export default async function generateServer (extraConfig: Partial<ConstructorParameters<typeof Server>[0]> = {}) {
	// -------------------------------------------------
	// Prepare config
	// -------------------------------------------------
	
	await config.fetchEnv("testing", true, true);
	
	// -------------------------------------------------
	// Prepare server
	// -------------------------------------------------
	
	// get configs
	await import ("../../config/paths");
	await import ("../../config/database");
	await import ("../../config/provider");
	await import ("../../config/middleware");
	
	const server = new Server({
		filePrefix	: config.config.paths.controllers,
		viewPrefix	: config.config.paths.view,
		hostname	: config.env.APP_HOSTNAME 		|| "0.0.0.0",
		port		: parseInt(config.env.APP_PORT) || 8080,
		...extraConfig,
	});
	
	// fetch into server
	server.addProviders(config.config.providers 	as ProviderInterface[]);
	server.addMiddlewares(config.config.middleware 	as Record<string, MiddlewareType>);
	server.addGlobals(config.config.global 			as MiddlewareType[]);
	
	// -------------------------------------------------
	// Return server
	// -------------------------------------------------
	
	return server;
}