import { CustomException } from "@acai/utils"

export default class ValidationException extends CustomException {
	public constructor (errors: Record<string, unknown>) {
		super("validation", "Validation error", errors);
	}
}