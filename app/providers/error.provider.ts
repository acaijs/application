// Packages
import { ErrorProvider as BaseErrorProvider } from "@acai/utils";

export default class ErrorProvider extends BaseErrorProvider {
	public logStorage = "storage/logs";
}