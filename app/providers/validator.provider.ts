// Packages
import { response }	from "@acai/server";

export default class ValidatorProvider {
	public onError ({error}) {
		if (error.type === "validation") {
			return response().status(403).data(error.data);
		}
	}
}
