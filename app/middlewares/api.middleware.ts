export default async function ApiMiddleware (request: AppRequest, next: (r: AppRequest) => Promise<AppRequest>) {
	return next({...request, headers: { ...request.headers, "accept": "application/json", "content-type": "application/json" } });
} 