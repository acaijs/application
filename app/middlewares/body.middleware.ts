// Packages
import { buildBodyParserMiddleware } from "@acai/utils";

export default buildBodyParserMiddleware();